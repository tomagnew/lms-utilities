﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace test_create_jsonp
{
    class Program
    {
        static void Main(string[] args)
        {
            var programs = new
            {
                container = "container-1",
                programs = new List<program> {
                    new program{
                        id=1,
                        name="PROGRAM_1"
                    },
                    new program{
                        id=2,
                        name="PROGRAM_2"
                    }
                }
            };
            // serialize JSON directly to a file
            var path = Directory.GetCurrentDirectory() + @"\data.jsonp";
            using (StreamWriter file = File.CreateText(path))
            {
                file.WriteLine("callback(");
                JsonSerializer serializer = new JsonSerializer();
                serializer.Formatting = Formatting.Indented;
                serializer.Serialize(file, programs);
                file.WriteLine(");");
            }
            Console.WriteLine(JsonConvert.SerializeObject(programs, Formatting.Indented));

        }
    }

    class program
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}

﻿using System;
using System.IO;
using System.IO.Compression;

namespace test_zip_compression
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("creating zip file from folder");
            string startPath = @".\folderToZip";
            string zipPath = @".\ZipFiles\result.zip";
            if (File.Exists(zipPath))
            {
                File.Delete(zipPath);
            }
            ZipFile.CreateFromDirectory(startPath, zipPath);
        }
    }
}
